package id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon;

public class Sword extends Weapon {

        public Sword() {
		this.weaponName = "Sword";
		this.weaponValue = 25;
		this.weaponDescription = "Great Sword"; 
	}
}
