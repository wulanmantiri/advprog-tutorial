package id.ac.ui.cs.advprog.tutorial3.composite.core;

import java.util.ArrayList;
import java.util.List;

public class OrdinaryMember implements Member {
	private String name;
	private String role;

	public OrdinaryMember(String name, String role) {
		this.name = name;
		this.role = role;
	}

    public String getName() {
		return this.name;
	}

    public String getRole() {
		return this.role;
	}

    public void addChildMember(Member member) {
		// add nothing
	}

    public void removeChildMember(Member member) {
		// remove nothing
	}

    public List<Member> getChildMembers() {
		return new ArrayList<Member>();
	}

}
