package id.ac.ui.cs.advprog.tutorial3.decorator.core.enhancer;

import id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon.Weapon;

import java.util.Random;

public class MagicUpgrade extends Weapon {

    Weapon weapon;

    public MagicUpgrade(Weapon weapon) {

        this.weapon= weapon;
    }

    @Override
    public String getName() {
        return weapon.getName();
    }

    // Senjata bisa dienhance hingga 15-20 ++
    @Override
    public int getWeaponValue() {
        Random random = new Random();
        int value = random.nextInt(6) + 15;
        return value + weapon.getWeaponValue();
    }

    @Override
    public String getDescription() {
        return weapon.getDescription() + "\n-> Magic Upgrade";
    }
}
