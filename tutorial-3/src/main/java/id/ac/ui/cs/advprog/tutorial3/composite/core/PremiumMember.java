package id.ac.ui.cs.advprog.tutorial3.composite.core;

import java.util.ArrayList;
import java.util.List;

public class PremiumMember implements Member {
	private String name;
	private String role;
	private List<Member> childMembers;

	public PremiumMember(String name, String role) {
		this.name = name;
		this.role = role;
		this.childMembers = new ArrayList<>();
	}

    public String getName() {
		return this.name;
	}

    public String getRole() {
		return this.role;
	}

    public void addChildMember(Member member) {
		if(role.equals("Master")) {
			childMembers.add(member);
		} else if(childMembers.size() < 3) {
			childMembers.add(member);
		}
	}

    public void removeChildMember(Member member) {
		if(childMembers.contains(member)) 
			childMembers.remove(member);
	}

    public List<Member> getChildMembers() {
		return this.childMembers;
	}
}
