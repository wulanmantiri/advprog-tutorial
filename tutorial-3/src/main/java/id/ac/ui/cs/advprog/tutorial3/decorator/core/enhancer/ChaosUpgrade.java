package id.ac.ui.cs.advprog.tutorial3.decorator.core.enhancer;

import id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon.Weapon;

import java.util.Random;

public class ChaosUpgrade extends Weapon {

    Weapon weapon;

    public ChaosUpgrade(Weapon weapon) {

        this.weapon= weapon;
    }

    @Override
    public String getName() {
        return weapon.getName();
    }

    // Senjata bisa dienhance hingga 50-55 ++
    @Override
    public int getWeaponValue() {
        Random random = new Random();
        int value = random.nextInt(6) + 50;
        return value + weapon.getWeaponValue();
    }

    @Override
    public String getDescription() {
        return weapon.getDescription() + "\n-> Chaos Upgrade";
    }
}
