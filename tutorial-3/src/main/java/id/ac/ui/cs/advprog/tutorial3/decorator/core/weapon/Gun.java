package id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon;

public class Gun extends Weapon {
	
	public Gun() {
		this.weaponName = "Gun";
		this.weaponValue = 20;
		this.weaponDescription = "Automatic Gun"; 
	}
}
