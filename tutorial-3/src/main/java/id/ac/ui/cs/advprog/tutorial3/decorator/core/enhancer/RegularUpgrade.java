package id.ac.ui.cs.advprog.tutorial3.decorator.core.enhancer;

import id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon.Weapon;

import java.util.Random;

public class RegularUpgrade extends Weapon {

    Weapon weapon;

    public RegularUpgrade(Weapon weapon) {

        this.weapon= weapon;
    }

    @Override
    public String getName() {
        return weapon.getName();
    }

    // Senjata bisa dienhance hingga 1-5 ++
    @Override
    public int getWeaponValue() {
        Random random = new Random();
        int value = random.nextInt(5) + 1;
        return value + weapon.getWeaponValue();
    }

    @Override
    public String getDescription() {
        return weapon.getDescription() + "\n-> Regular Upgrade";
    }
}
