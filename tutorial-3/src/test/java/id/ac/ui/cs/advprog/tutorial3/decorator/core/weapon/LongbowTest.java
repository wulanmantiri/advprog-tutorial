package id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class LongbowTest {


    private Weapon weapon;

    @BeforeEach
    public void setUp(){
        weapon = new Longbow();
    }

    @Test
    public void testMethodGetWeaponName(){
        // Method getName = weapon.getDeclaredMethod("getName");

        // assertEquals("java.lang.String", getName.getGenericReturnType().getTypeName());
        assertEquals("Longbow", weapon.getName());
    }

    @Test
    public void testGetMethodWeaponDescription(){
        // Method getDescription = weapon.getDeclaredMethod("getDescription");

        // assertEquals("java.lang.String", getDescription.getGenericReturnType().getTypeName());
        assertEquals("Big Longbow", weapon.getDescription());
    }

    @Test
    public void testMethodGetWeaponValue(){
        assertEquals(15, weapon.getWeaponValue());
    }
}
