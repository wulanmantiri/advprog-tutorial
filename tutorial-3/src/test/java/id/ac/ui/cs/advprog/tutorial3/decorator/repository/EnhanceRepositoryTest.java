package id.ac.ui.cs.advprog.tutorial3.decorator.repository;

import id.ac.ui.cs.advprog.tutorial3.decorator.core.enhancer.EnhancerDecorator;
import id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class EnhanceRepositoryTest {

    ArrayList<Weapon> weapons = new ArrayList<>();
    Weapon weapon;
    @BeforeEach
    public void setUp(){
        weapons.add(new Gun());
        weapons.add(new Sword());
        weapons.add(new Shield());
        weapons.add(new Longbow());
    }

    @Test
    public void testMethodEnhanceToAllWeapons(){

        for (Weapon weapon: weapons) {

            if (weapon.getName().equals("Gun")){

                Weapon gun;
                gun = WeaponProducer.WEAPON_GUN.createWeaponEnhancer();
                gun = EnhancerDecorator.RAW_UPGRADE.addWeaponEnhancement(gun);
                gun = EnhancerDecorator.REGULAR_UPGRADE.addWeaponEnhancement(gun);
                int index = weapons.indexOf(weapon);
                weapons.set(index,gun);
                assertTrue(weapon.getWeaponValue() < gun.getWeaponValue());

            } else if(weapon.getName().equals("Longbow")) {

                Weapon longbow;
                longbow = WeaponProducer.WEAPON_LONGBOW.createWeaponEnhancer();
                longbow = EnhancerDecorator.REGULAR_UPGRADE.addWeaponEnhancement(longbow);
                longbow = EnhancerDecorator.MAGIC_UPGRADE.addWeaponEnhancement(longbow);
                longbow = EnhancerDecorator.UNIQUE_UPGRADE.addWeaponEnhancement(longbow);
                int index = weapons.indexOf(weapon);
                weapons.set(index,longbow);
                assertTrue(weapon.getWeaponValue() < longbow.getWeaponValue());

            } else if(weapon.getName().equals("Shield")) {

                Weapon shield;
                shield = WeaponProducer.WEAPON_SHIELD.createWeaponEnhancer();
                shield = EnhancerDecorator.RAW_UPGRADE.addWeaponEnhancement(shield);
                shield = EnhancerDecorator.MAGIC_UPGRADE.addWeaponEnhancement(shield);
                int index = weapons.indexOf(weapon);
                weapons.set(index,shield);
                assertTrue(weapon.getWeaponValue() < shield.getWeaponValue());

            } else if(weapon.getName().equals("Sword")) {

                Weapon sword;
                sword = WeaponProducer.WEAPON_SWORD.createWeaponEnhancer();
                sword = EnhancerDecorator.UNIQUE_UPGRADE.addWeaponEnhancement(sword);
                sword = EnhancerDecorator.MAGIC_UPGRADE.addWeaponEnhancement(sword);
                int index = weapons.indexOf(weapon);
                weapons.set(index,sword);
                assertTrue(weapon.getWeaponValue() < sword.getWeaponValue());
            }
        }
    }
}
