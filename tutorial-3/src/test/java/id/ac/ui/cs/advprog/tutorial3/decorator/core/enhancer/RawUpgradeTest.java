package id.ac.ui.cs.advprog.tutorial3.decorator.core.enhancer;

import id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon.Longbow;
import id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon.Shield;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Random;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class RawUpgradeTest {

    private RawUpgrade rawUpgrade;

    @BeforeEach
    public void setUp(){
        rawUpgrade = new RawUpgrade(new Shield());
    }

    @Test
    public void testMethodGetWeaponName(){
        // Method getName = rawUpgrade.getDeclaredMethod("getName");

        // assertEquals("java.lang.String", getName.getGenericReturnType().getTypeName());
        assertEquals("Shield", rawUpgrade.getName());
    }

    @Test
    public void testGetMethodWeaponDescription(){
        // Method getDescription = rawUpgrade.getDeclaredMethod("getDescription");

        // assertEquals("java.lang.String", getDescription.getGenericReturnType().getTypeName());
        String weaponDesc = new Shield().getDescription();
        assertEquals(weaponDesc + "\n-> Raw Upgrade", rawUpgrade.getDescription());
    }

    @Test
    public void testMethodGetWeaponValue(){
        int newValue = rawUpgrade.getWeaponValue();
        int oldValue = new Shield().getWeaponValue();
        assertTrue(newValue >= 5 + oldValue
            && newValue <= 10 + oldValue);
    }
}
