package id.ac.ui.cs.advprog.tutorial3.composite.core;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class PremiumMemberTest {
    private Member member;

    @BeforeEach
    public void setUp() {
        member = new PremiumMember("Wati", "Gold Merchant");
    }

    @Test
    public void testMethodGetName() {
        // Method getName = member.getDeclaredMethod("getName");

        // assertEquals("java.lang.String", getName.getGenericReturnType().getTypeName());
        assertEquals("Aqua", member.getName());
    }

    @Test
    public void testMethodGetRole() {
        // Method getRole = member.getDeclaredMethod("getRole");

        // assertEquals("java.lang.String", getRole.getGenericReturnType().getTypeName());
        assertEquals("Goddess", member.getRole());
    }

    @Test
    public void testMethodAddChildMember() {
        member.addChildMember(new OrdinaryMember("Asuna", "Waifu"));
        assertEquals(1, member.getChildMembers().size());
    }

    @Test
    public void testMethodRemoveChildMember() {
        Member asuna = new OrdinaryMember("Asuna", "Waifu");
        member.addChildMember(asuna);
        member.removeChildMember(asuna);
        assertEquals(0, member.getChildMembers().size());
    }

    @Test
    public void testNonGuildMasterCanNotAddChildMembersMoreThanThree() {
        member.addChildMember(new OrdinaryMember("Asuna", "Waifu"));
        member.addChildMember(new OrdinaryMember("Wulan", "Mahasiswa"));
        member.addChildMember(new OrdinaryMember("Lue", "Reflector"));
        member.addChildMember(new OrdinaryMember("X", "Invalid Member"));
        assertEquals(3, member.getChildMembers().size());
    }

    @Test
    public void testGuildMasterCanAddChildMembersMoreThanThree() {
        Member master = new PremiumMember("Wulan", "Master");
        master.addChildMember(new OrdinaryMember("Asuna", "Waifu"));
        master.addChildMember(new OrdinaryMember("Sasuke", "Uchiha"));
        master.addChildMember(new OrdinaryMember("Lue", "Reflector"));
        master.addChildMember(new OrdinaryMember("Aqua", "Goddess"));
        assertEquals(4, master.getChildMembers().size());
    }
}
