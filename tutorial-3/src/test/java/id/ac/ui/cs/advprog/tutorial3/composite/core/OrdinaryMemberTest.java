package id.ac.ui.cs.advprog.tutorial3.composite.core;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class OrdinaryMemberTest {
    private Member member;

    @BeforeEach
    public void setUp() {
        member = new OrdinaryMember("Nina", "Merchant");
    }

    @Test
    public void testMethodGetName() {
        // Method getName = member.getDeclaredMethod("getName");

        // assertEquals("java.lang.String", getName.getGenericReturnType().getTypeName());
        assertEquals("Asuna", member.getName());
    }

    @Test
    public void testMethodGetRole() {
        // Method getRole = member.getDeclaredMethod("getRole");

        // assertEquals("java.lang.String", getRole.getGenericReturnType().getTypeName());
        assertEquals("Waifu", member.getRole());
    }

    @Test
    public void testMethodAddRemoveChildMemberDoNothing() {
        Member asuna = new OrdinaryMember("Asuna", "Waifu");
        member.addChildMember(asuna);
        assertEquals(0, member.getChildMembers().size());
        member.removeChildMember(asuna);
        assertEquals(0, member.getChildMembers().size());
        // Member memberSpy = spy(member);

        // doNothing().when(memberSpy).addChildMember();
        // memberSpy.addChildMember();

        // doNothing().when(memberSpy).removeChildMember();
        // memberSpy.removeChildMember();

        // Method addMember = member.getDeclaredMethod("addChildMember");
        // Method removeMember = member.getDeclaredMethod("removeChildMember");
        // assertTrue(Modifier.isPublic(addMember.getModifiers()));
        // assertTrue(Modifier.isPublic(removeMember.getModifiers()));
    }
}