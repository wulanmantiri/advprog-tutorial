package id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class ShieldTest {

    private Weapon weapon;

    @BeforeEach
    public void setUp(){
        weapon = new Shield();
    }

    @Test
    public void testMethodGetWeaponName(){
        // Method getName = weapon.getDeclaredMethod("getName");

        // assertEquals("java.lang.String", getName.getGenericReturnType().getTypeName());
        assertEquals("Shield", weapon.getName());
    }

    @Test
    public void testGetMethodWeaponDescription(){
        // Method getDescription = weapon.getDeclaredMethod("getDescription");

        // assertEquals("java.lang.String", getDescription.getGenericReturnType().getTypeName());
        assertEquals("Heater Shield", weapon.getDescription());
    }

    @Test
    public void testMethodGetWeaponValue(){
        assertEquals(10, weapon.getWeaponValue());
    }
}
