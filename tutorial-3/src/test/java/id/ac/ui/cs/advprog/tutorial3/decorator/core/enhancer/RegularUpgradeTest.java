package id.ac.ui.cs.advprog.tutorial3.decorator.core.enhancer;

import id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon.Sword;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Random;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class RegularUpgradeTest {

    private RegularUpgrade regularUpgrade;

    @BeforeEach
    public void setUp(){
        regularUpgrade = new RegularUpgrade(new Sword());
    }

    @Test
    public void testMethodGetWeaponName(){
        // Method getName = regularUpgrade.getDeclaredMethod("getName");

        // assertEquals("java.lang.String", getName.getGenericReturnType().getTypeName());
        assertEquals("Sword", regularUpgrade.getName());
    }

    @Test
    public void testGetMethodWeaponDescription(){
        // Method getDescription = regularUpgrade.getDeclaredMethod("getDescription");

        // assertEquals("java.lang.String", getDescription.getGenericReturnType().getTypeName());
        String weaponDesc = new Sword().getDescription();
        assertEquals(weaponDesc + "\n-> Regular Upgrade", regularUpgrade.getDescription());
    }

    @Test
    public void testMethodGetWeaponValue(){
        int newValue = regularUpgrade.getWeaponValue();
        int oldValue = new Sword().getWeaponValue();
        assertTrue(newValue >= 1 + oldValue
            && newValue <= 5 + oldValue);
    }
}
