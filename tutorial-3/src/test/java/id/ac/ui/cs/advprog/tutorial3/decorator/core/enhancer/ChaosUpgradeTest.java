package id.ac.ui.cs.advprog.tutorial3.decorator.core.enhancer;


import id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon.Gun;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Random;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class ChaosUpgradeTest {

    private ChaosUpgrade chaosUpgrade;
    @BeforeEach
    public void setUp(){
        chaosUpgrade = new ChaosUpgrade(new Gun());
    }

    @Test
    public void testMethodGetWeaponName(){
        // Method getName = chaosUpgrade.getDeclaredMethod("getName");

        // assertEquals("java.lang.String", getName.getGenericReturnType().getTypeName());
        assertEquals("Gun", chaosUpgrade.getName());
    }

    @Test
    public void testGetMethodWeaponDescription(){
        // Method getDescription = chaosUpgrade.getDeclaredMethod("getDescription");

        // assertEquals("java.lang.String", getDescription.getGenericReturnType().getTypeName());
        String weaponDesc = new Gun().getDescription();
        assertEquals(weaponDesc + "\n-> Chaos Upgrade", chaosUpgrade.getDescription());
    }

    @Test
    public void testMethodGetWeaponValue(){
        int newValue = chaosUpgrade.getWeaponValue();
        int oldValue = new Gun().getWeaponValue();
        assertTrue(newValue >= 50 + oldValue
            && newValue <= 55 + oldValue);
    }

}
