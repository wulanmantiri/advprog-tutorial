package id.ac.ui.cs.advprog.tutorial3.composite.core;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class GuildTest {
    private Guild guild;
    private Member guildMaster;

    @BeforeEach
    public void setUp() {
        guildMaster = new PremiumMember("Eko", "Master");
        guild = new Guild(guildMaster);
    }

    @Test
    public void testMethodAddMember() {
        Member sasuke = new OrdinaryMember("Sasuke", "Uchiha");
        guild.addMember(guildMaster, sasuke);
        assertEquals(1, guildMaster.getChildMembers().size());
        //verify(guildMaster, times(1)).addChildMember(sasuke);
    }

    @Test
    public void testMethodRemoveMember() {
        Member sasuke = new OrdinaryMember("Sasuke", "Uchiha");
        guild.addMember(guildMaster, sasuke);
        guild.removeMember(guildMaster, sasuke);
        assertEquals(0, guildMaster.getChildMembers().size());
        //verify(guildMaster, times(1)).removeChildMember(sasuke);
    }

    @Test
    public void testMethodMemberHierarchy() {
        Member sasuke = new OrdinaryMember("Asep", "Servant");
        guild.addMember(guildMaster, sasuke);
        assertEquals(guildMaster, guild.getMemberHierarchy().get(0));
        assertEquals(sasuke, guild.getMemberHierarchy().get(0).getChildMembers().get(0));
    }

    @Test
    public void testMethodGetMember() {
        Member sasuke = new OrdinaryMember("Sasuke", "Uchiha");
        guild.addMember(guildMaster, sasuke);
        assertEquals(sasuke, guild.getMember("Sasuke", "Uchiha"));
        //when(guild.getMember("Sasuke", "Uchiha")).thenReturn(sasuke);
    }
}
