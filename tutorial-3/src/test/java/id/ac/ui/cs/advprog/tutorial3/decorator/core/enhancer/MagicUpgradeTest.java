package id.ac.ui.cs.advprog.tutorial3.decorator.core.enhancer;

import id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon.Longbow;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Random;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class MagicUpgradeTest {


    private MagicUpgrade magicUpgrade;

    @BeforeEach
    public void setUp(){
        magicUpgrade = new MagicUpgrade(new Longbow());
    }

    @Test
    public void testMethodGetWeaponName(){
        // Method getName = magicUpgrade.getDeclaredMethod("getName");

        // assertEquals("java.lang.String", getName.getGenericReturnType().getTypeName());
        assertEquals("Longbow", magicUpgrade.getName());
    }

    @Test
    public void testGetMethodWeaponDescription(){
        // Method getDescription = magicUpgrade.getDeclaredMethod("getDescription");

        // assertEquals("java.lang.String", getDescription.getGenericReturnType().getTypeName());
        String weaponDesc = new Longbow().getDescription();
        assertEquals(weaponDesc + "\n-> Magic Upgrade", magicUpgrade.getDescription());
    }

    @Test
    public void testMethodGetWeaponValue(){
        int newValue = magicUpgrade.getWeaponValue();
        int oldValue = new Longbow().getWeaponValue();
        assertTrue(newValue >= 15 + oldValue
            && newValue <= 20 + oldValue);
    }
}
