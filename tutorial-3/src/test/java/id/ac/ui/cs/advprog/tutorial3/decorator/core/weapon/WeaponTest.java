package id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import static org.junit.jupiter.api.Assertions.*;

public class WeaponTest {

    Weapon weapon;

    @BeforeEach
    public void setUp(){
        weapon = Mockito.mock(Weapon.class);
    }

    @Test
    public void testMethodGetWeaponName(){
        Mockito.doCallRealMethod().when(weapon).getName();
        String weaponName = weapon.getName();
        Mockito.when(weapon.getName()).thenReturn(weaponName);
    }

    @Test
    public void testMethodGetWeaponDescription(){
        Mockito.doCallRealMethod().when(weapon).getDescription();
        String weaponDescription = weapon.getDescription();
        Mockito.when(weapon.getDescription()).thenReturn(weaponDescription);
    }

    @Test
    public void testMethodGetWeaponValue(){
        Mockito.doCallRealMethod().when(weapon).getWeaponValue();
        int weaponValue = weapon.getWeaponValue();
        Mockito.when(weapon.getWeaponValue()).thenReturn(weaponValue);
    }
}
