package id.ac.ui.cs.advprog.tutorial3.decorator.core.enhancer;

import id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon.Shield;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Random;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class UniqueUpgradeTest {

    private UniqueUpgrade uniqueUpgrade;

    @BeforeEach
    public void setUp(){
        uniqueUpgrade = new UniqueUpgrade(new Shield());
    }

    @Test
    public void testMethodGetWeaponName(){
        // Method getName = uniqueUpgrade.getDeclaredMethod("getName");

        // assertEquals("java.lang.String", getName.getGenericReturnType().getTypeName());
        assertEquals("Shield", uniqueUpgrade.getName());
    }

    @Test
    public void testGetMethodWeaponDescription(){
        // Method getDescription = uniqueUpgrade.getDeclaredMethod("getDescription");

        // assertEquals("java.lang.String", getDescription.getGenericReturnType().getTypeName());
        String weaponDesc = new Shield().getDescription();
        assertEquals(weaponDesc + "\n-> Unique Upgrade", uniqueUpgrade.getDescription());
    }

    @Test
    public void testMethodGetWeaponValue(){
        int newValue = uniqueUpgrade.getWeaponValue();
        int oldValue = new Shield().getWeaponValue();
        assertTrue(newValue >= 10 + oldValue
            && newValue <= 15 + oldValue);
    }
}
