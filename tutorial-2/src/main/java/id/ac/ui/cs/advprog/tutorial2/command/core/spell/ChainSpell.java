package id.ac.ui.cs.advprog.tutorial2.command.core.spell;

import java.util.ArrayList;

public class ChainSpell implements Spell {
    protected ArrayList<Spell> arrSpells;

    public ChainSpell(ArrayList<Spell> arr) {
        this.arrSpells = arr;
	}

    @Override
    public void cast() {
        for(Spell spell: arrSpells) {
            spell.cast();
        }
    }

    @Override
    public void undo() {
        for(int i = arrSpells.size() - 1; i >= 0; i--) {
            arrSpells.get(i).undo();
        }
    }

    @Override
    public String spellName() {
        return "ChainSpell";
    }
}
