# Tutorial 4

[![pipeline status](https://gitlab.com/wulanmantiri_/advprog-tutorial/badges/tutorial-4/pipeline.svg)](https://gitlab.com/wulanmantiri_/advprog-tutorial/-/commits/tutorial-4) [![coverage report](https://gitlab.com/wulanmantiri_/advprog-tutorial/badges/tutorial-4/coverage.svg)](https://gitlab.com/wulanmantiri_/advprog-tutorial/-/commits/tutorial-4)

Anda berjalan pada kegelapan malam, berteman lilin kecil yang dipinjamkan guild kepada anda. Tubuh anda bersisihan lirih dengan angin malam. Tatapan
anda jauh ke depan, ke dalam kegelapan di tengah kota. 

Suasana sangat sepi. Tidak banyak orang yang berlalu-lalang memang. Kebanyakan sudah lelap dalam tidur. Seharusnya tidak akan ada orang
lagi selarut ini. Itulah yang anda cari, sebuah ketenangan setelah pekerjaan yang panjang. Anda benar-benar berharap tidak bertemu siapapun.  
Namun, sepertinya bukan itu yang terjadi. 

Di tengah jembatan kota yang gelap, sesosok manusia duduk. Seolah menunggu sesuatu. Ia terlihat seolah memain-mainkan mana di tangannya. 
Kehadirannya terasa aneh. Anda merasa dia ada, tapi tidak berada di dimensi yang sama. Auranya begitu kuat sehingga terasa lemah bagi orang 
yang hanya berlalu-lalang. 

Anda merasa tidak nyaman dengan kehadirannya dan mencoba mengabaikannya. Anda berjalan balik, menjauhi jembatan gelap dengan sosoknya sedang menatap
air. Bulu kuduk anda berdiri. Semuanya terasa semakin dingin dan hitam. Anda mencoba mengabaikan perasaan itu dan mempecepat langkah...

" Saya sudah menunggu anda pahlawan.."

Sosoknya tiba-tiba muncul di depan wajah anda, membuat detak jantung anda menjadi sangat cepat. Ketika anda melihat wajahnya dengan jelas, anda kemudian menyadari
identitas orang di hadapan anda.

" [] (Empty List)" tutur anda kosong.

" Saya sudah mendengar cerita tentang anda pahlawan.. kali ini, saya membutuhkan bantuan anda tuan pahlawan. "

Anda tidak menyadari kehadirannya dari awal karena dia terasa seperti orang lain, berbeda dengan saat ia memanggil anda ke dunia ini. 
Gemuruh emosi menggelora di kepala anda. Banyak hal yang ingin anda tanyakan padanya. Anda juga ingin menolak permintaannya. Namun, anda sadar. 

*Anda tidak punya kekuatan atau pun pilihan untuk itu..*



# Here Comes the Holy Grail and the Knight Factory
[] menginkan bantuan anda mengelola sebuah 'academy' yang ia buat untuk para ksatria. Banyak pemuda yang datang untuk menjadi ksatria ke tempat ini.
Awalnya tidak ada yang salah dengan tempat ini. "Sepertinya hanya perasaan saja bahwa [] memiliki sisi aneh." Lagi pula dia adalah orang yang memanggil anda
ke dunia ini, tidak mungkin dia melakukan sesuatu yang buruk. Setidaknya itu yang anda harapkan saat ini.  

## Abstract Factory
- - - - 
Terdapat 2 Academy yang ingin [] anda bantu yaitu `DrangleicAcademy` dan `LordranAcademy`.

Setiap Academy akan memberikan beberapa teknik weaponry (`ShiningBuster` dan `ThousandJacker`), armory (`MetalArmor` dan `ShiningArmor`, dan skill (`ShiningForce` dan `ThousandYearsOfPain`).
  
Setiap academy menawarkan ksatria untuk bisa memiliki spesialisasi menjadi `MajesticKnight`, `MetalClusterKnight`, atau `SyntheticKnight`.

`MajesticKnight` memiliki spesialisasi pada **armor** dan **weapon**.
`MetalClusterKnight` memiliki spesialisasi pada **armor** dan **skill**.
`SyntheticKnight` memiliki spesialisasi pada **weapon** dan **skill**.

Artinya, bila tidak ada komponen yang bukan merupakan bagian dari jenis **knight**, maka jenis **knight** tidak perlu menyertakan komponen tersebut.

### TODO Abstract Factory Pattern
- [ ] Mengimplementasikan semua implementasi yang dibutuhkan pada class-class dari package  `core/upgrade`.
- [ ] Mengimplementasikan semua implementasi yang dibutuhkan pada class-class dari package  `core/armory`.
- [ ] Mengimplementasikan semua implementasi yang dibutuhkan pada class `core/DrangleicAcademy.java`.
- [ ] Mengimplementasikan semua implementasi yang dibutuhkan pada class `core/LodranAcademy.java`.
- [ ] Mengimplementasikan semua implementasi yang dibutuhkan pada class `repository/AcademyRepository.java`.
- [ ] Mengimplementasikan semua implementasi yang dibutuhkan pada class `service/AcademyServiceImpl.java`.
- [ ] Lengkapilah seluruh method test untuk `core` dan `service`. Lakukan perubahan pada badge pipeline readme sesuai dengan pengaturan repo anda.

## Singleton
- - - -

Anda merasakan keanehan saat  bekerja dengan [] di academy. Kenapa setiap orang yang menjadi ksatria memiliki kepribadian berbeda dengan mereka sebelumnya.
Mereka terasa seperti orang yang sama sekali berbeda. Bahkan mereka seolah tidak memiliki ingatan tentang siapa mereka sebelumnya. Anda merasakan keanehan dari tempat ini.
Anda merasa para ksatria itu seperti anda. Orang-orang yang ingatannya diambil. Apa maksud semua ini?

Ketika anda mencari jawaban anda melihat jalan menuju suatu ruangan rahasia. Anda menatap suatu cawan yang bersinar di ujung ruangan itu. 
Dengan melihatnya saja, anda sadar kalau benda itu bukan benda biasa. Kekuatan yang ada pada benda itu diluar pemahaman manusia biasa. 
Ketika anda menyentuhnya, pengetahuan yang anda tidak miliki sebelumnya masuk ke dalam pikiran anda.  

*Anda menyadari siapa [] sebenarnya..*

Ia adalah seorang raja. Kerajaan yang ia miliki mendapat krisis besar. Kebanyakan penduduk telah hilang karena kematian massal akibat wabah. 
Bahkan kerajaannya tidak dapat berjalan dengan semestinya lagi. Bangsawan dan penduduk biasa tidak ada perbedaan. Semuanya kelaparan, semuanya kehilangan,
semuanya menderita. Di dalam keputusasaannya, di dalam tangisan panjangnya, [] menemukan Holy Grail. Sebuah cawan suci yang dapat mengabulkan
keinginannya. Sebuah harapan buat masa depan kerajaannya. 

Dengan kekuatan itu, ia menggunakannya untuk mengatur kerajaan, bahkan dunia. Ia mencoba menjadi seorang pelindung dengan menggunakan kekuatannya.
Mengendalikan ingatan manusia. Ia berhasil mencapai perdamaian dunia. Namun, kekuatan itu menggerogotinya. Ia selalu ingin lebih. Ia ingin lebih banyak kekuatan. 
Hal itu yang mendorongnya menjadi orang yang seperti ini.

“You want to win… You want to win so desperately? You desire the Grail so desperately? You have crushed… You have crushed my sole remaining wish… And you just stand there…do you not feel any shame at all?! I will never forgive you… I will never forgive any of you! You inhuman monsters, who have ruined the honor of a knight, let my blood taint your dreams forever! May the Grail be cursed. May the wish it grants bring disaster! And when you fall into the searing pits of hell!” - Dying Curse

Suatu suara menggelora di kepala anda, suara yang menyakitkan. Penuh kegelapan dan kebencian. 
Cawan berbahaya itu ada di hadapan anda. Anda melihat akan banyak konflik kepentingan jika benda ini dapat memiliki multiple instance. Anda mengingat sebuah design pattern yang dapat menyelesaikan ini.
Singleton Pattern. Dengan determinasi yang anda miliki, anda mencoba melakukan sesuatu.

Anda tahu anda bukanlah seorang pahlawan yang dianugerahi banyak kekuatan. Anda bukan seorang penyihir yang mampu mengubah dunia seorang diri dengan kekuatan anda. 
Anda sadar bahwa anda adalah programmer. Jika bukan membuat kode, apa lagi yang bisa dilakukan. Itulah peran anda di dunia ini. Dengan semangat itu anda mulai
membuat program demi masa depan dunia..


### TODO Singleton Pattern
- [ ] Mengimplementasikan semua implementasi yang dibutuhkan pada class  `core/HolyWish.java`. (note: untuk implementasi *singleton pattern* ini anda bebas menggunakan approach yang mana, apakah **eager** atau **lazy**, silahkan berikan keterangan). 
- [ ] Mengimplementasikan semua implementasi yang dibutuhkan pada class  `service/HolyGrail.java`. 
- [ ] Lengkapilah seluruh method test untuk `core` dan `service`. Lakukan perubahan pada badge pipeline readme sesuai dengan pengaturan repo anda. (note: tambahkan test yang khusus untuk test hanya satu instance yang terbuat pada test di singleton pattern ini).

## Summary
Tugas kalian pada 2 pattern di atas adalah melengkapi tiap implementasi yang ditandai oleh `//TODO`
dan membuat test untuk `core` dan `service` pada masing-masing pattern.
