package id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.armory.armor;

public class MetalArmor implements Armor {

    @Override
    public String getName() {
        return "Metal Armor";
    }

    @Override
    public String getDescription() {
        return "Armor";
    }
}
