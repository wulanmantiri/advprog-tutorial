package id.ac.ui.cs.advprog.tutorial4.abstractfactory.core;

import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.armory.Armory;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.armory.LordranArmory;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.upgrade.Knight;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.upgrade.MajesticKnight;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.upgrade.MetalClusterKnight;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.upgrade.SyntheticKnight;

public class LordranAcademy extends KnightAcademy {

    @Override
    public String getName() {
        return "Lordran";
    }

    @Override
    public Knight produceKnight(String type) {
        Knight knight = null;
        Armory armory = new LordranArmory();

        switch (type) {
            case "majestic":
                knight = new MajesticKnight(armory); 
                knight.setName("Majestic Knight");
                break;
            case "metal cluster":
                knight = new MetalClusterKnight(armory);    
                knight.setName("Metal Cluster Knight");
                break;
            case "synthetic":
                knight = new SyntheticKnight(armory); 
                knight.setName("Synthetic Knight");
                break;
        }

        return knight;
    }
}
