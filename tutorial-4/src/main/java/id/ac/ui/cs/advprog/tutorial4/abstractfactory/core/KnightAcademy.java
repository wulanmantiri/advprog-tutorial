package id.ac.ui.cs.advprog.tutorial4.abstractfactory.core;

import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.upgrade.Knight;

public abstract class KnightAcademy {

    public abstract Knight produceKnight(String type);

    public abstract String getName();

    public Knight getKnight(String type) {
        Knight knight = produceKnight(type);
        knight.prepare();
        return knight;
    }
}
