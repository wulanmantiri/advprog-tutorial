package id.ac.ui.cs.advprog.tutorial4.abstractfactory.service;

import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.DrangleicAcademy;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.KnightAcademy;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.LordranAcademy;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.armory.DrangleicArmory;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.upgrade.Knight;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.upgrade.MajesticKnight;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.repository.AcademyRepository;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;
import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class AcademyServiceImplTest {

    @Mock
    private AcademyRepository academyRepository;

    private AcademyServiceImpl academyServiceImpl;

    public void setUp() {
        this.academyServiceImpl = new AcademyServiceImpl(academyRepository);
        verify(academyServiceImpl, times(1)).seed();
    }

    public void getKnightAcademiesShouldReturnAcademyRepositoryGetKnightAcademies() {
        List<KnightAcademy> academyList = new ArrayList<>();
        academyList.add(new DrangleicAcademy());
        academyList.add(new LordranAcademy());

        when(academyServiceImpl.getKnightAcademies())
                .thenReturn(academyList);

        Iterable<KnightAcademy> calledAcademy = academyRepository.getKnightAcademies();
        assertEquals(academyList, calledAcademy);
    }

    public void produceKnightShouldCallKnightInAcademyRepository() {        
        academyServiceImpl.produceKnight("Drangleic", "majestic");
        verify(academyRepository, times(1)).getKnightAcademyByName("Drangleic");
        assertTrue(academyServiceImpl.getKnight() instanceof Knight);
    }

    public void seedShouldCallRepositoryAddKnightAcademy() {
        academyServiceImpl.seed();
        verify(academyRepository, times(1)).addKnightAcademy("Lordran", new LordranAcademy());
        verify(academyRepository, times(1)).addKnightAcademy("Drangleic", new DrangleicAcademy());

        assertEquals(2, academyServiceImpl.getKnightAcademies().size());
    }
}
