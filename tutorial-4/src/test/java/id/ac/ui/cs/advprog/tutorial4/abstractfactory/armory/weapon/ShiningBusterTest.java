package id.ac.ui.cs.advprog.tutorial4.abstractfactory.armory.weapon;

import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.armory.weapon.ShiningBuster;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.armory.weapon.Weapon;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class ShiningBusterTest {

    Weapon shiningBuster;

    @BeforeEach
    public void setUp(){
        shiningBuster = new ShiningBuster();
    }

    @Test
    public void testToString(){
        assertEquals("Shining Buster", shiningBuster.getName());
    }

    @Test
    public void testDescription(){
        assertEquals("Weapon", shiningBuster.getDescription());
    }
}
