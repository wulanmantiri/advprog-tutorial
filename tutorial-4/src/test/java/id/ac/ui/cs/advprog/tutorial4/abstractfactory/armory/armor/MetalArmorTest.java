package id.ac.ui.cs.advprog.tutorial4.abstractfactory.armory.armor;

import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.armory.armor.Armor;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.armory.armor.MetalArmor;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class MetalArmorTest {

    Armor metalArmor;

    @BeforeEach
    public void setUp(){
        metalArmor = new MetalArmor();
    }

    @Test
    public void testToString(){
        assertEquals("Metal Armor", metalArmor.getName());
    }

    @Test
    public void testDescription(){
        assertEquals("Armor", metalArmor.getDescription());
    }
}
