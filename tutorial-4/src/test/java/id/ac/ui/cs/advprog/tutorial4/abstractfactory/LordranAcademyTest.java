package id.ac.ui.cs.advprog.tutorial4.abstractfactory;

import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.LordranAcademy;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.armory.LordranArmory;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.armory.armor.ShiningArmor;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.armory.skill.ShiningForce;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.armory.weapon.ShiningBuster;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.KnightAcademy;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.upgrade.MajesticKnight;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.upgrade.Knight;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.upgrade.MetalClusterKnight;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.upgrade.SyntheticKnight;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;


import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;


public class LordranAcademyTest {
    KnightAcademy lordranAcademy;
    Knight majesticKnight;
    Knight metalClusterKnight;
    Knight syntheticKnight;

    @BeforeEach
    public void setUp() {
        this.lordranAcademy = new LordranAcademy();
        this.majesticKnight = lordranAcademy.getKnight("majestic");
        this.metalClusterKnight = lordranAcademy.getKnight("metal cluster");
        this.syntheticKnight = lordranAcademy.getKnight("synthetic");
    }

    @Test
    public void checkName() {
        assertEquals("Lordran", lordranAcademy.getName());
    }

    @Test
    public void checkProduceKnight() {
        String type = "majestic";
        Knight knight = lordranAcademy.produceKnight(type);
        if(type.equals("majestic")) {
            assertTrue(knight instanceof MajesticKnight);
            assertEquals("Majestic Knight", knight.getName());    
        } else if(type.equals("metal cluster")) {
            assertTrue(knight instanceof MetalClusterKnight);
            assertEquals("Metal Cluster Knight", knight.getName());    
        } else if(type.equals("synthetic")) {
            assertTrue(knight instanceof MajesticKnight);
            assertEquals("Synthetic Knight", knight.getName());    
        }
    }

    @Test
    public void checkKnightInstances() {
        assertTrue(majesticKnight instanceof MajesticKnight);
        assertTrue(metalClusterKnight instanceof MetalClusterKnight);
        assertTrue(syntheticKnight instanceof SyntheticKnight);
    }

    @Test
    public void checkKnightNames() {
        assertEquals("Majestic Knight", majesticKnight.getName());
        assertEquals("Metal Cluster Knight", metalClusterKnight.getName());
        assertEquals("Synthetic Knight", syntheticKnight.getName());
    }

    @Test
    public void checkKnightDescriptions() {
        assertTrue(syntheticKnight.getArmor() == null);
        assertNotNull(syntheticKnight.getWeapon());
        assertNotNull(syntheticKnight.getSkill());
        assertTrue(syntheticKnight.getWeapon() instanceof ShiningBuster);
        assertTrue(syntheticKnight.getSkill() instanceof ShiningForce);

        assertNotNull(metalClusterKnight.getArmor());
        assertTrue(metalClusterKnight.getWeapon() == null);
        assertNotNull(metalClusterKnight.getSkill());
        assertTrue(metalClusterKnight.getArmor() instanceof ShiningArmor);
        assertTrue(metalClusterKnight.getSkill() instanceof ShiningForce);
        
        assertNotNull(majesticKnight.getArmor());
        assertNotNull(majesticKnight.getWeapon());
        assertTrue(majesticKnight.getSkill() == null);
        assertTrue(majesticKnight.getArmor() instanceof ShiningArmor);
        assertTrue(majesticKnight.getWeapon() instanceof ShiningBuster);
    }
}
