package id.ac.ui.cs.advprog.tutorial4.singleton.service;

import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import id.ac.ui.cs.advprog.tutorial4.singleton.core.HolyWish;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class HolyGrailTest {

    private HolyWish holyWish;
    private HolyGrail holyGrail;

    public void setUp() {
        this.holyGrail = new HolyGrail();
        assertTrue(this.holyGrail instanceof HolyGrail);
        this.holyWish = holyGrail.getHolyWish();
        assertTrue(this.holyWish instanceof HolyWish);
    }

    public void makeAWishShouldCallHolyWishSetWish() {
        holyGrail.makeAWish("AdProg A :D");
        verify(holyWish, times(1)).setWish("AdProg A :D");
    }

    public void getHolyWishShouldReturnHolyWishInstance() { 
        HolyWish calledWish = HolyWish.getInstance();  
        // HolyGrail grailSpy = spy(holyGrail);
        // when(grailSpy.getHolyWish()).thenReturn(holyWish);
        assertEquals(calledWish, holyGrail.getHolyWish());
    }

}
