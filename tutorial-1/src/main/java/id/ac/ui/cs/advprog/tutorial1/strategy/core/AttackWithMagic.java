package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class AttackWithMagic implements AttackBehavior {
	public String attack() {
		return "Attack With Magic";
	}

	public String getType() {
		return "Magic";
	}
}
